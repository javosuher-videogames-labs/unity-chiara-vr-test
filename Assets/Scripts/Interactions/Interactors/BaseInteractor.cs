using Chiara.Interactions.Interactables;
using UnityEngine;

namespace Chiara.Interactions.Interactors
{
    /// <summary>
    /// Abstract class with the implementation of the basic interactor functionalities to inherit 
    /// </summary>
    public abstract class BaseInteractor : MonoBehaviour
    {
		#region Methods
        
        /// <summary>
        /// The method used to perform the interaction to an interactable object 
        /// </summary>
        /// <param name="objectTransform">The interactable object transform</param>
        protected void Interact(Transform objectTransform) => Interact(objectTransform.gameObject);
        
        /// <summary>
        /// The method used to perform the interaction to an interactable object 
        /// </summary>
        /// <param name="gameObject"></param>
        protected void Interact(GameObject gameObject)
        {
            IInteractable interactable = gameObject.GetComponent<IInteractable>();
            if (interactable != null)
                interactable.Interact();
        }
        
        /// <summary>
        /// Method to check if an object is interactable
        /// </summary>
        /// <param name="gameObject">The object to check if is interactable</param>
        /// <returns>True if is an IInteractable object</returns>
        protected bool IsInteractable(GameObject gameObject)
        {
            IInteractable interactable = gameObject.GetComponent<IInteractable>();
            return interactable != null;
        }
        
        #endregion
    }
}