using Chiara.Controllers;
using UnityEngine;
using UnityEngine.Assertions;

namespace Chiara.Interactions.Interactors
{
    /// <summary>
    /// The camera reticule interactor used by the player to interact in the scene
    /// This class uses the CameraReticuleController to perform the interaction
    /// </summary>
    [RequireComponent(typeof(CameraReticuleController))]
    public class CameraReticuleInteractor : BaseInteractor
    {
		#region Consts
        
        private const float MAX_RAYCAST_DISTANCE = 50;
        
        #endregion
        
		#region Fields
        
        [Header("Main Parameters")]
        [SerializeField] private CameraReticuleController _cameraReticuleController;

        private GameObject _interactableObject;
        
        #endregion
        
        #region Unity Methods

        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        private void Start()
        {
            Assert.IsNotNull(_cameraReticuleController);
            
            _cameraReticuleController.reticuleFillEndEvent.AddListener(Interact);
        }

        /// <summary>
        /// Update is called each frame per second
        /// </summary>
        private void Update()
        {
            RaycastInteraction();
        }

        /// <summary>
        /// Reset is called in editor when the script is attached 
        /// </summary>
        private void Reset()
        {
            _cameraReticuleController = GetComponentInChildren<CameraReticuleController>();
        }
        
        #endregion
        
        #region Methods

        /// <summary>
        /// This method perform a raycast to hit a IInteractable object to do the interaction
        /// </summary>
        private void RaycastInteraction()
        {
            // Perform the raycast
            if (Physics.Raycast(_cameraReticuleController.transform.position,
                    _cameraReticuleController.transform.forward,
                    out RaycastHit hit, MAX_RAYCAST_DISTANCE) && IsInteractable(hit.transform.gameObject))
            {
                if (_interactableObject == null || hit.transform.gameObject != _interactableObject)
                {
                    _interactableObject = hit.transform.gameObject;
                    _cameraReticuleController.StartReticuleFill();
                }
            }
            else
            {
                _interactableObject = null;
                _cameraReticuleController.StopReticuleFill();
            }
        }

        /// <summary>
        /// Method to interact with the interactable object selected by the raycast
        /// </summary>
        private void Interact()
        {
            Interact(_interactableObject);
            _interactableObject = null;
        }
        
        #endregion
    }
}
