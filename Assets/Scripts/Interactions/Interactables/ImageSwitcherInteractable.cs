using System.Linq;
using Chiara.Controllers;
using Chiara.Network;
using Chiara.Serializables;
using UnityEngine;
using UnityEngine.Events;

namespace Chiara.Interactions.Interactables
{
    /// <summary>
    /// The interactable script to load 3 images in the scene from config json files
    /// </summary>
    public class ImageSwitcherInteractable : MonoBehaviour, IInteractable
    {
		#region Consts
        
        private const string IMAGE_URI_FORMAT = ".png";
        
        #endregion
        
		#region Fields
        
        [Header("Main Parameters")]
        [SerializeField] private SpriteRenderer _spriteRenderer1; 
        [SerializeField] private SpriteRenderer _spriteRenderer2; 
        [SerializeField] private SpriteRenderer _spriteRenderer3;
        
        [Header("Events")]
        public UnityEvent interactionEvent; 

        private JsonSceneConfiguration _jsonSceneConfiguration;
        private JsonPlaceholderItems _jsonPlaceholderItems;
        
		#endregion
        
		#region IInteractable Methods
    
        /// <summary>
        /// The method which is called by the interactor to perform a behaviour
        /// </summary>
        public void Interact()
        {
            SetImages();
            interactionEvent?.Invoke();
        }
        
		#endregion
        
		#region Methods

        /// <summary>
        /// Method called to configure the images using the scene configuration json
        /// </summary>
        public void JsonSceneConfiguration()
        {
            // Get the json from the main configuration controller
            _jsonSceneConfiguration = SceneConfigurationController.Instance.jsonSceneConfiguration;

            // Configure all images positions
            ConfigureImagePositionAndRotation(_spriteRenderer1.transform, _jsonSceneConfiguration.image1);
            ConfigureImagePositionAndRotation(_spriteRenderer2.transform, _jsonSceneConfiguration.image2);
            ConfigureImagePositionAndRotation(_spriteRenderer3.transform, _jsonSceneConfiguration.image3);
            
            SetImages();
        }

        /// <summary>
        /// Configure a sprite renderer location and rotation
        /// </summary>
        /// <param name="imageTransform">The sprite renderer image</param>
        /// <param name="jsonImage">The json image data</param>
        private void ConfigureImagePositionAndRotation(Transform imageTransform, JsonSceneConfigurationImage jsonImage)
        {
            imageTransform.position = 
                new Vector3(jsonImage.position[0], jsonImage.position[1], jsonImage.position[2]);
            imageTransform.rotation =
                Quaternion.Euler(new Vector3(jsonImage.rotation[0], jsonImage.rotation[1], jsonImage.rotation[2]));
        }

        /// <summary>
        /// Method responsible to download and set the images to the sprite renderers configured
        /// This use the first three images from the configured API
        /// The images are shuffled to set a new image each time
        /// </summary>
        private void SetImages()
        {
            // Get the images from the API
            WebRequests.Instance.GetJsonRequest<JsonPlaceholderItems>(
                _jsonSceneConfiguration.jsonPlaceholderImageAPIUrl,
                jsonPlaceholderItems =>
                {
                    _jsonPlaceholderItems = jsonPlaceholderItems;
                    
                    // Randomize the array to get a different image each time
                    System.Random random = new System.Random();
                    _jsonPlaceholderItems.jsonPlaceholderItems = _jsonPlaceholderItems.jsonPlaceholderItems
                        .OrderBy(x => random.Next()).ToArray();
                        
                    // Get only the first three images
                    JsonPlaceholderItem jsonPlaceholderItem1 = _jsonPlaceholderItems.jsonPlaceholderItems[0];
                    JsonPlaceholderItem jsonPlaceholderItem2 = _jsonPlaceholderItems.jsonPlaceholderItems[1];
                    JsonPlaceholderItem jsonPlaceholderItem3 = _jsonPlaceholderItems.jsonPlaceholderItems[2];

                    // Get the textures from the API to set in the sprite renderers
                    WebRequests.Instance.GetTextureRequest(jsonPlaceholderItem1.url + IMAGE_URI_FORMAT,
                        texture2D => SetImage(_spriteRenderer1, texture2D));
                    WebRequests.Instance.GetTextureRequest(jsonPlaceholderItem2.url + IMAGE_URI_FORMAT,
                        texture2D => SetImage(_spriteRenderer2, texture2D));
                    WebRequests.Instance.GetTextureRequest(jsonPlaceholderItem3.url + IMAGE_URI_FORMAT,
                        texture2D => SetImage(_spriteRenderer3, texture2D));
                });
        }

        /// <summary>
        /// Set a texture to the sprite renderers
        /// </summary>
        /// <param name="image">The sprite renderer image</param>
        /// <param name="texture">The texture to apply in the sprite renderer</param>
        private void SetImage(SpriteRenderer image, Texture2D texture)
        {
            image.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), 
                new Vector2(0.5f, 0.5f));
        }
        
        #endregion
    }
}
