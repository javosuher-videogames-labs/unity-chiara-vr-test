namespace Chiara.Interactions.Interactables
{
    /// <summary>
    /// Main interface to config a interactable behaviour triggered by an interactor
    /// </summary>
    public interface IInteractable
    {
        /// <summary>
        /// The method which is called by the interactor to perform a behaviour
        /// </summary>
        void Interact();
    }
}