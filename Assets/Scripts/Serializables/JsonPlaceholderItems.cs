using System;
using UnityEngine;

namespace Chiara.Serializables
{
    /// <summary>
    /// The placeholder json array with the data of images from the API 
    /// </summary>
    [Serializable]
    public class JsonPlaceholderItems : IJsonSerialization<JsonPlaceholderItems>
    {
		#region Fields
        
        public JsonPlaceholderItem[] jsonPlaceholderItems;
        
        #endregion
        
        #region IJsonSerialization Methods
        
        /// <summary>
        /// Serialize the json
        /// </summary>
        /// <returns>The implemented class serialized as string</returns>
        public string Serialize() => JsonUtility.ToJson(jsonPlaceholderItems);

        /// <summary>
        /// Deserialize the json
        /// </summary>
        /// <param name="json">The json string</param>
        /// <returns>The implemented class</returns>
        public JsonPlaceholderItems Deserialize(string json)
        {
            JsonPlaceholderItems jsonObject = JsonUtility.FromJson<JsonPlaceholderItems>(
                "{\"jsonPlaceholderItems\":" + json + "}");
            
            jsonPlaceholderItems = jsonObject.jsonPlaceholderItems;
            return this;
        }
        
        #endregion
    }
}