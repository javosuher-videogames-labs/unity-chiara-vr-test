namespace Chiara.Serializables
{
    /// <summary>
    /// Interface implemented to serialize and deserialize json classes
    /// </summary>
    public interface IJsonSerialization<out T> 
    {
        /// <summary>
        /// Serialize the json
        /// </summary>
        /// <returns>The implemented class serialized as string</returns>
        string Serialize();
        
        /// <summary>
        /// Deserialize the json
        /// </summary>
        /// <param name="json">The json string</param>
        /// <returns>The implemented class</returns>
        T Deserialize(string json);
    }
}