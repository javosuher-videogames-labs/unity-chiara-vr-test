using System;
using UnityEngine;

namespace Chiara.Serializables
{
    /// <summary>
    /// The json scene configuration 
    /// </summary>
    [Serializable]
    public class JsonSceneConfiguration : IJsonSerialization<JsonSceneConfiguration>
    {
		#region Fields
        
        public string jsonPlaceholderImageAPIUrl;
        public JsonSceneConfigurationImage image1;
        public JsonSceneConfigurationImage image2;
        public JsonSceneConfigurationImage image3;
        
        #endregion
        
        /// <summary>
        /// Serialize the json
        /// </summary>
        /// <returns>The implemented class serialized as string</returns>
        public string Serialize() => JsonUtility.ToJson(this);
        
        /// <summary>
        /// Deserialize the json
        /// </summary>
        /// <param name="json">The json string</param>
        /// <returns>The implemented class</returns>
        public JsonSceneConfiguration Deserialize(string json)
        {
            JsonSceneConfiguration jsonObject = JsonUtility.FromJson<JsonSceneConfiguration>(json);

            jsonPlaceholderImageAPIUrl = jsonObject.jsonPlaceholderImageAPIUrl;
            image1 = jsonObject.image1;
            image2 = jsonObject.image2;
            image3 = jsonObject.image3;
            return this;
        }
    }

    /// <summary>
    /// The image position and rotation of the scene configuration
    /// </summary>
    [Serializable]
    public class JsonSceneConfigurationImage
    {
		#region Fields
        
        public float[] position;
        public float[] rotation;
        
        #endregion
    }
}