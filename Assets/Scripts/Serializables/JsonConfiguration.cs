using System;
using UnityEngine;

namespace Chiara.Serializables
{
    /// <summary>
    /// The class of the main configuration json 
    /// </summary>
    [Serializable]
    public class JsonConfiguration : IJsonSerialization<JsonConfiguration>
    {
		#region Fields
        
        public string sceneConfigurationJsonUrl;
        
        #endregion
        
        #region IJsonSerialization Methods
        
        /// <summary>
        /// Serialize the json
        /// </summary>
        /// <returns>The implemented class serialized as string</returns>
        public string Serialize() => JsonUtility.ToJson(this);
        
        /// <summary>
        /// Deserialize the json
        /// </summary>
        /// <param name="json">The json string</param>
        /// <returns>The implemented class</returns>
        public JsonConfiguration Deserialize(string json)
        {
            JsonConfiguration jsonObject = JsonUtility.FromJson<JsonConfiguration>(json);

            sceneConfigurationJsonUrl = jsonObject.sceneConfigurationJsonUrl;
            return this;
        }
        
        #endregion
    }
}