using System;

namespace Chiara.Serializables
{
    /// <summary>
    /// The placeholder json item with the data of an image 
    /// </summary>
    [Serializable]
    public class JsonPlaceholderItem
    {
		#region Fields
        
        public int albumId;
        public int id;
        public string title;
        public string url;
        public string thumbnailUrl;
        
        #endregion
    }
}