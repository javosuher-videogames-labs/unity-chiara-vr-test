using UnityEngine;
using UnityEngine.Assertions;

namespace Chiara.GameObjects
{
    /// <summary>
    /// This class rotate the GameObject to look always to the camera 
    /// </summary>
    public class LookAtCamera : MonoBehaviour
    {
		#region Fields
        
        private Transform _cameraTransform;
        
        #endregion
        
        #region Unity Methods
        
        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        private void Start()
        {
            Camera camera = Camera.main;
            if (camera != null) 
                _cameraTransform = camera.transform;
            
            Assert.IsNotNull(_cameraTransform);
        }

        /// <summary>
        /// Update is called each frame per second
        /// </summary>
        private void Update()
        {
            transform.rotation = Quaternion.LookRotation(transform.position - _cameraTransform.position);
        }
        
        #endregion
    }
}