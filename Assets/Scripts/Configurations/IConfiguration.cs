namespace Chiara.Configurations
{
    /// <summary>
    /// The interface which should inherit the configuration scripts 
    /// </summary>
    public interface IConfiguration
    {
        /// <summary>
        /// This method is called to configure specific implemented configuration
        /// </summary>
        void Configure();
    }
}