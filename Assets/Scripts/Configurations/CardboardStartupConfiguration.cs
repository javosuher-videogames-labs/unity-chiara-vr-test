using Google.XR.Cardboard;
using UnityEngine;

namespace Chiara.Configurations
{
    /// <summary>
    /// Initializes Cardboard XR Plugin
    /// </summary>
    public class CardboardStartupConfiguration : IConfiguration
    {
        #region IConfiguration Methods
        
        /// <summary>
        /// This method is called to configure specific implemented configuration
        /// </summary>
        public void Configure()
        {
            // Configures the app to not shut down the screen and sets the brightness to maximum
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            Screen.brightness = 1.0f;

            // Checks if the device parameters are stored and scans them if not
            if (!Api.HasDeviceParams())
                Api.ScanDeviceParams();
        }
        
        #endregion
    }
}