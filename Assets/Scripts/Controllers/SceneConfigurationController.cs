using Chiara.Network;
using Chiara.Patterns;
using Chiara.Serializables;
using UnityEngine;
using UnityEngine.Events;

namespace Chiara.Controllers
{
    /// <summary>
    /// The scene main configurator of the application scene
    /// </summary>
    public class SceneConfigurationController : Singleton<SceneConfigurationController>
    {
		#region Consts
        
        private const string CONFIGURATION_JSON_NAME = "ConfigurationJson";
        
        #endregion
        
		#region Fields
        
        [Header("Events")]
        public UnityEvent jsonSceneConfigurationLoadedEvent;
        
        [HideInInspector] public JsonConfiguration jsonConfiguration;
        [HideInInspector] public JsonSceneConfiguration jsonSceneConfiguration;
        
        #endregion
        
        #region Unity Methods
        
        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        private void Start()
        {
            GetConfigJsons();
        }
        
        #endregion
        
        #region Methods

        /// <summary>
        /// Method responsible to get the main configuration json
        /// </summary>
        private void GetConfigJsons()
        {
            // Get the application configuration json
            TextAsset json = Resources.Load<TextAsset>(CONFIGURATION_JSON_NAME);
            jsonConfiguration = new JsonConfiguration();
            jsonConfiguration.Deserialize(json.text);
            
            // Get the scene configuration json
            WebRequests.Instance.GetJsonRequest<JsonSceneConfiguration>(jsonConfiguration.sceneConfigurationJsonUrl, 
                jsonSceneConfiguration =>
                {
                    this.jsonSceneConfiguration = jsonSceneConfiguration;
                    jsonSceneConfigurationLoadedEvent?.Invoke();
                });
        }
        
        #endregion
    }
}