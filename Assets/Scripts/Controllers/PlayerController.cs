using System.Collections.Generic;
using Chiara.Configurations;
using Chiara.Inputs;
using UnityEngine;

namespace Chiara.Controllers
{
    /// <summary>
    /// The player controller containing its main configurations and inputs 
    /// </summary>
    public class PlayerController : MonoBehaviour
    {
		#region Fields
        
        private List<IConfiguration> _playerConfigurations = new List<IConfiguration>();
        private List<IInputs> _playerInputs = new List<IInputs>();
        
        #endregion
        
        #region Unity Methods
        
        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        private void Start()
        {
            // The platform selection using the preprocessed macros
            #if UNITY_EDITOR
            _playerInputs.Add(new EditorPlayerInputs());
            #else
            _playerConfigurations.Add(new CardboardStartupConfiguration());
            _playerInputs.Add(new CardboardPlayerInputs());
            #endif

            // Execute configurations and inputs
            foreach (IConfiguration playerConfiguration in _playerConfigurations)
                playerConfiguration.Configure();
            foreach (IInputs playerInput in _playerInputs)
                playerInput.Initialize();
        }

        /// <summary>
        /// Update is called each frame per second
        /// </summary>
        private void Update()
        {
            foreach (IInputs playerInput in _playerInputs)
                playerInput.PlayerInput();
        }
        
        #endregion
    }
}