using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Chiara.Controllers
{
    /// <summary>
    /// The camera reticule controller to perform its functionalities 
    /// </summary>
    public class CameraReticuleController : MonoBehaviour
    {
		#region Consts

        private const float RETICULE_FILL_EMPTY = 0f;
        
        #endregion
        
		#region Fields
        
        [Header("Main Parameters")]
        [SerializeField] private Image _mainReticule;
        [SerializeField] private Image _reticleFill;
        [SerializeField] private float _reticleFillMaxTime = 5f;

        [Header("Events")]
        public UnityEvent reticuleFillStartEvent; 
        public UnityEvent reticuleFillEndEvent; 
        public UnityEvent reticuleFillCancelEvent; 

        private Coroutine _reticuleFillCoroutine;
        
        #endregion

        #region Unity Methods

        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        public void Start()
        {
            // Set both visible
            _mainReticule.gameObject.SetActive(true);
            _reticleFill.gameObject.SetActive(true);

            // Config reticule fill
            _reticleFill.fillAmount = RETICULE_FILL_EMPTY;
        }
        
        #endregion
        
        #region Methods

        /// <summary>
        /// Method responsible to start the reticule fill process 
        /// </summary>
        public void StartReticuleFill()
        {
            if(_reticuleFillCoroutine != null)
                StopCoroutine(_reticuleFillCoroutine);
            _reticuleFillCoroutine = StartCoroutine(StartReticuleFillProcess(_reticleFillMaxTime));
        }

        /// <summary>
        /// Method responsible to stop the reticule fill process 
        /// </summary>
        public void StopReticuleFill()
        {
            if (_reticuleFillCoroutine != null)
            {
                StopCoroutine(_reticuleFillCoroutine);
                _reticuleFillCoroutine = null;
                _reticleFill.fillAmount = RETICULE_FILL_EMPTY;
                reticuleFillCancelEvent?.Invoke();
            }
        }

        /// <summary>
        /// The reticule fill process coroutine
        /// </summary>
        /// <param name="maxTime">The maximum time of the reticule</param>
        private IEnumerator StartReticuleFillProcess(float maxTime = 5f)
        {
            // Initialization
            float currentTime = 0f;
            _reticleFill.fillAmount = RETICULE_FILL_EMPTY;
            
            // Process
            reticuleFillStartEvent?.Invoke();
            while (currentTime < maxTime)
            {
                _reticleFill.fillAmount = currentTime / maxTime;
                currentTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            _reticleFill.fillAmount = RETICULE_FILL_EMPTY;
            reticuleFillEndEvent?.Invoke();
        }
        
        #endregion
    }
}