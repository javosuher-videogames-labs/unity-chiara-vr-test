using UnityEngine;
using UnityEngine.Assertions;

namespace Chiara.Inputs
{
    /// <summary>
    /// The editor player input to move the camera using the mouse
    /// </summary>
    public class EditorPlayerInputs : IInputs 
    {
		#region Consts
        
        private const float MOUSE_SENTIVITY = 0.1f;
        
        #endregion
        
		#region Fields
        
        private Vector3 _pressPosition;
        private Transform _cameraTransform;
        
        #endregion
        
        #region IInputs Methods
        
        /// <summary>
        /// This method initialize the inputs implementation
        /// </summary>
        public void Initialize()
        {
            Camera camera = Camera.main;
            if (camera != null) 
                _cameraTransform = camera.transform;
            
            Assert.IsNotNull(_cameraTransform);
        }
        
        /// <summary>
        /// This method process the inputs implementation
        /// </summary>
        public void PlayerInput()
        {
            if (Input.GetMouseButtonDown(0))
                _pressPosition = Input.mousePosition;
 
            if (Input.GetMouseButton(0))
            {
                Vector3 direction = Input.mousePosition - _pressPosition;
                Vector3 cameraPosition = _cameraTransform.position;
                
                _cameraTransform.RotateAround(cameraPosition, Vector3.up, 
                    direction.x * -MOUSE_SENTIVITY); 
                _cameraTransform.RotateAround(cameraPosition, _cameraTransform.right, 
                    direction.y * MOUSE_SENTIVITY); 
                _pressPosition = Input.mousePosition;
            }
        }
        
        #endregion
    }
}