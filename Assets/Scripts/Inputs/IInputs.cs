namespace Chiara.Inputs
{
    /// <summary>
    /// The interface which should inherit the inputs configuration scripts 
    /// </summary>
    public interface IInputs
    {
        /// <summary>
        /// This method initialize the inputs implementation
        /// </summary>
        void Initialize();
        
        /// <summary>
        /// This method process the inputs implementation
        /// </summary>
        void PlayerInput();
    }
}