using Google.XR.Cardboard;
using UnityEngine;

namespace Chiara.Inputs
{
    /// <summary>
    /// Cardboard main inputs
    /// </summary>
    public class CardboardPlayerInputs : IInputs
    {
        #region IInputs Methods
        
        /// <summary>
        /// This method initialize the inputs implementation
        /// </summary>
        public void Initialize() { }
        
        /// <summary>
        /// This method process the inputs implementation
        /// </summary>
        public void PlayerInput()
        {
            if (Api.IsGearButtonPressed)
                Api.ScanDeviceParams();

            if (Api.IsCloseButtonPressed)
                Application.Quit();

            if (Api.IsTriggerHeldPressed)
                Api.Recenter();

            if (Api.HasNewDeviceParams())
                Api.ReloadDeviceParams();

            Api.UpdateScreenParams();
        }
        
        #endregion
    }
}