using System;
using System.Collections;
using Chiara.Patterns;
using Chiara.Serializables;
using UnityEngine;
using UnityEngine.Networking;

namespace Chiara.Network
{
    /// <summary>
    /// The web request class to perform request to a APIs
    /// Uses the Singleton interface to an easy use in code
    /// </summary>
    public class WebRequests : Singleton<WebRequests>
    {
        #region Methods

        /// <summary>
        /// Get request to obtain a specific json and deserialize to a desired class
        /// </summary>
        /// <param name="uri">The uri where the json is</param>
        /// <param name="onSuccess">Callback executed when the json is downloaded</param>
        /// <param name="onError">Callback executed when the request fails</param>
        /// <typeparam name="T">The json deserialization type</typeparam>
        public void GetJsonRequest<T>(string uri, Action<T> onSuccess,
            Action<string> onError = null) where T : IJsonSerialization<T>, new() =>
            StartCoroutine(GetJsonRequestCoroutine<T>(uri, onSuccess, onError));
        
        /// <summary>
        /// Get request to obtain a specific texture to use 
        /// </summary>
        /// <param name="uri">The uri where the texture is</param>
        /// <param name="onSuccess">Callback executed when the texture is downloaded</param>
        /// <param name="onError">Callback executed when the request fails</param>
        public void GetTextureRequest(string uri, Action<Texture2D> onSuccess,
            Action<string> onError = null) =>
            StartCoroutine(GetTextureRequestCoroutine(uri, onSuccess, onError));

        /// <summary>
        /// Get request Coroutine to obtain a specific json and deserialize to a desired class
        /// </summary>
        /// <param name="uri">The uri where the json is</param>
        /// <param name="onSuccess">Callback executed when the json is downloaded</param>
        /// <param name="onError">Callback executed when the request fails</param>
        /// <typeparam name="T">The json deserialization type</typeparam>
        private IEnumerator GetJsonRequestCoroutine<T>(string uri, Action<T> onSuccess, 
            Action<string> onError = null) where T : IJsonSerialization<T>, new()
        {
            UnityWebRequest unityWebRequest = UnityWebRequest.Get(uri);
            yield return unityWebRequest.SendWebRequest();
            
            switch (unityWebRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                case UnityWebRequest.Result.ProtocolError:
                    string errorMessage = "[" + uri + "] Error: " + unityWebRequest.error;
                    Debug.LogError(errorMessage);
                    onError?.Invoke(errorMessage);
                    break;
                case UnityWebRequest.Result.Success:
                    T jsonSerialization = new T();
                    jsonSerialization.Deserialize(unityWebRequest.downloadHandler.text);
                    onSuccess?.Invoke(jsonSerialization);
                    break;
            }
        }

        /// <summary>
        /// Get request coroutine to obtain a specific texture to use 
        /// </summary>
        /// <param name="uri">The uri where the texture is</param>
        /// <param name="onSuccess">Callback executed when the texture is downloaded</param>
        /// <param name="onError">Callback executed when the request fails</param>
        private IEnumerator GetTextureRequestCoroutine(string uri, Action<Texture2D> onSuccess, Action<string> onError = null)
        {
            UnityWebRequest unityWebRequest = UnityWebRequestTexture.GetTexture(uri);
            yield return unityWebRequest.SendWebRequest();
            
            switch (unityWebRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                case UnityWebRequest.Result.ProtocolError:
                        string errorMessage = "[" + uri + "] Error: " + unityWebRequest.error;
                    Debug.LogError(errorMessage);
                    onError?.Invoke(errorMessage);
                    break;
                case UnityWebRequest.Result.Success:
                    Texture2D texture = ((DownloadHandlerTexture) unityWebRequest.downloadHandler).texture;
                    onSuccess?.Invoke(texture);
                    break;
            }
        }
        
        #endregion
    }
}