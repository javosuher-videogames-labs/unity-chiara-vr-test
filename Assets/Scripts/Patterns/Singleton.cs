﻿using UnityEngine;

namespace Chiara.Patterns
{
	/// <summary>
	/// A Singleton pattern implementation to the MonoBehaviour scripts in project to an easy access 
	/// </summary>
	/// <typeparam name="T">MonoBehaviour inheritor</typeparam>
	public abstract class Singleton<T> : MonoBehaviour where T : Component
	{
		#region Fields

		/// <summary>
		/// The instance
		/// </summary>
		private static T instance;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the instance
		/// </summary>
		/// <value>The instance</value>
		public static T Instance
		{
			get
			{
				if (instance == null)
				{
					instance = FindObjectOfType<T>();
					if (instance == null)
					{
						GameObject obj = new GameObject();
						obj.name = typeof(T).Name;
						instance = obj.AddComponent<T>();
					}
				}

				return instance;
			}
		}

		#endregion

		#region Methods

        /// <summary>
        /// Start is called before the Start 
        /// </summary>
		protected virtual void Awake()
		{
			if (instance == null)
			{
				instance = this as T;
				DontDestroyOnLoad(gameObject);
			}
			else
			{
				Destroy(gameObject);
			}
		}

		#endregion
	}
}